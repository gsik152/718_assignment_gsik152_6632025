Bulls & Cows game result.
Your code: 1234
Computer's code: 2579
---
Turn 1:
You guessed 7354, scoring 0 bulls and 2 cows
Computer guessed 0839, scoring 1 bull and 0 cows
---
Turn 2:
You guessed 1234, scoring 0 bulls and 1 cow
Computer guessed 7320, scoring 0 bulls and 2 cows
---
Turn 3:
You guessed 1587, scoring 1 bull and 1 cow
Computer guessed 2658, scoring 0 bulls and 1 cow
---
Turn 4:
You guessed 2568, scoring 2 bulls and 0 cows
Computer guessed 2609, scoring 0 bulls and 1 cow
---
Turn 5:
You guessed 2978, scoring 2 bulls and 1 cow
Computer guessed 9754, scoring 1 bull and 0 cows
---
Turn 6:
You guessed 2579, scoring 4 bulls and 0 cows
You win! :)