public class BullCowGame {
    //This class is responsible for the main operation of the game
    public static void main(String[] args) {
        BullCowGame bcg = new BullCowGame();
        bcg.start();
    }

    private void start() {
        while (true) { //Runs until the user decides to exit
            System.out.println("Select your difficulty level");
            System.out.println("----------------------------");
            System.out.println("1. Easy");
            System.out.println("2. Medium");
            System.out.println("3. Hard");
            System.out.println("4. Exit");
            System.out.println();
            String difficulty = Keyboard.readInput();
            if (difficulty.equals("1") || difficulty.toLowerCase().equals("easy")) { //Checks user input to execute correct game
                new EasyAI().easyAI(); //Instantiates the easyAI class
            } else if (difficulty.equals("2") || difficulty.toLowerCase().equals("medium")) {
                new MediumAI().mediumAI(); //Instantiates the mediumAI class
            } else if (difficulty.equals("3") || difficulty.toLowerCase().equals("hard")) {
                new HardAI().hardAI(); //Instantiates the hardAI class
            } else if (difficulty.equals("4") || difficulty.toLowerCase().equals("exit")) {
                System.out.println("Thanks for playing!");
                break; //Leaves the game
            } else {
                System.out.println("Invalid input! Try again"); //Validates user input
            }
        }
    }
}
