import java.util.ArrayList;
import java.util.List;

public class MediumAI extends BullCowGameInputOutput {
    //This class inherits all the methods of the BullCowGameInputOutput class
    public void mediumAI(){
        // This methods executes the medium version of the game
        System.out.println("Please enter your secret code:");
        String userCode = getUserInput(); //Obtains the users' secret code
        System.out.println("---");
        String computerCode = computerNumber(); //Obtain the computers' secret code
        List<String> guessList = new ArrayList<>(); //For tracking computer guess history
        List<String> guesses = fileChecker(); //See if the user wants to use guesses from a file
        List<String> userGuesses = new ArrayList<>(); //For tracking user guess history
        List<int[]> userResults = new ArrayList<>(); //For tracking user result history
        List<int[]> computerResults = new ArrayList<>(); //For tracking computer result history
        int turns = 0; //For tracking the number of turns
        int gameTurns = getTurns(); //For checking how many turns the user wants
        for (int i = 0; i < gameTurns; i++) {
            //Game is running depending on number of turns user wants
            String playerGuess = "";
            playerGuess = guessLooper(guesses, playerGuess, computerCode, i); //Obtains player guess in appropriate way
            if (playerGuess.equals("NMG")) { //If a file is used this will be returned if no more guesses are available
                playerGuess = playerGuess(computerCode); //We ask the player to input their own guess
            }
            userGuesses.add(playerGuess); //The players' guess is added to the history
            userResults.add(checker(playerGuess, computerCode)); //The result of the guess is added to the history
            if (playerGuess.equals(computerCode)) { //If the player has guessed correctly, the game ends
                System.out.println("You win! :)");
                break;
            }
            System.out.println();
            String computerGuess = computerNumber(); //The computer guess is obtained
            while (guessList.contains(computerGuess)) {
                /* The computer guess is checked against the existing guess history. If it has already been guessed,
                 * a new guess is produced, which is checked again. We keep producing guesses until we have one
                 * that has not already been produced
                 */
                String newGuess = computerNumber();
                if (!newGuess.equals(computerGuess)) {
                    computerGuess = newGuess;
                    break;
                }
            }
            guessList.add(computerGuess); //The computer guess is added to history
            computerResults.add(checker(computerGuess, userCode)); //The result of the computer guess is added to history
            computerGuess(computerGuess, userCode); //The computer guess is checked and the results displayed
            if (computerGuess.equals(userCode)) { //If the computer has guessed correctly the game ends
                System.out.println("Computer wins! :(");
                break;
            }
            System.out.println("---");
            if (i == gameTurns-1) { //If the maximum number of turns is reached we have a draw
                System.out.println("Draw!");
                System.out.println();
            }
            turns++; //Increments the turn by 1, but only if no one has won yet
        }
        saveGame(userCode, computerCode, userGuesses, guessList, userResults, computerResults, turns);
        //The above passes the game history to the saveGame method which asks if the user wants it saved, and then does so.
    }
}
