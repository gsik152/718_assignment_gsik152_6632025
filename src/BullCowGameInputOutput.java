import java.io.*;
import java.util.ArrayList;
import java.util.List;

class BullCowGameInputOutput {
    //This class handles all the input and output required for the Bull and Cow game
    String getUserInput() { //This method gets a valid input from the user
        String userCode = Keyboard.readInput();
        char[] numbers = userCode.toCharArray(); //Converting the user input to an array to check for duplicates
        boolean duplicates = false;
        for (int i = 0; i < numbers.length; i++) { //Loops through the character array
            for (int j = i + 1 ; j < numbers.length; j++) { //Loops through every other character
                if (numbers[i] == numbers[j]) { //Compares to check for duplicates
                    duplicates = true;
                }
            }
        }
        while (duplicates || userCode.length() != 4) { //Checks if there are duplicates or if there are more or less than 4 digits
            //Performs same operation as above until user inputs valid number
            System.out.println("Invalid input! Each digit must be different and there must be 4 digits");
            userCode = Keyboard.readInput();
            numbers = userCode.toCharArray();
            duplicates = false;
            for (int i = 0; i < numbers.length; i++) {
                for (int j = i + 1 ; j < numbers.length; j++) {
                    if (numbers[i] == numbers[j]) {
                        duplicates = true;
                    }
                }
            }
        }
        return userCode;
    }

    String computerNumber() { //This method produces the computer's secret code, and returns it as a string.
        int dig1 = (int) (Math.random() * 10);
        int dig2 = (int) (Math.random() * 10);
        while (dig1 == dig2) { //Checks to ensure no duplicates
            dig2 = (int) (Math.random() * 10);
        }
        int dig3 = (int) (Math.random() * 10);
        while (dig3 == dig2 || dig3 == dig1) { //Checks to ensure no duplicates
            dig3 = (int) (Math.random() * 10);
        }
        int dig4 = (int) (Math.random() * 10);
        while (dig4 == dig1 || dig4 == dig2 || dig4 == dig3) { //Checks to ensure no duplicates
            dig4 = (int) (Math.random() * 10);
        }
        return (Integer.toString(dig1) + Integer.toString(dig2) + Integer.toString(dig3) + Integer.toString(dig4));
    }

    int[] checker(String guess, String code) { //This method checks how many 'bulls' and 'cows' we get from a guess
        int bulls = 0;
        int cows = 0;
        for (int i = 0; i < 4; i++) {
            if (guess.charAt(i) == code.charAt(i)) { //Checks if a character is the same and in the same position
                bulls++;
            } else if (code.contains(Character.toString(guess.charAt(i)))) { //Checks only if a character is contained
                cows++;
            }
        }
        return new int[]{bulls, cows}; //Returns an integer array containing the number of bulls and cows
    }

    String playerGuess(String computerCode) {
        //This method provides the output for the players' guess, when the player is inputting it directly
        System.out.print("You guess: ");
        String playerGuess = getUserInput(); //We obtain input from the player for their guess
        System.out.print("Result: ");
        if (checker(playerGuess, computerCode)[0] == 1) { //Checks to see if we have 1 bull or many bulls, so appropriate output is produced
            System.out.print("1 bull and ");
        } else {
            System.out.print(checker(playerGuess, computerCode)[0] + " bulls and ");
        }
        if (checker(playerGuess, computerCode)[1] == 1) { //Checks to see if we have 1 cow or many cows, so appropriate output is produced
            System.out.println("1 cow");
        } else {
            System.out.println(checker(playerGuess, computerCode)[1] + " cows");
        }
        return playerGuess;
    }

    private String playerGuess(String computerCode, String playerGuess) {
        //This method provides output for the players' guess, only where it has already been provided through a file
        System.out.println("You guess: " + playerGuess);
        System.out.print("Result: ");
        if (checker(playerGuess, computerCode)[0] == 1) { //Checks to see if we have 1 bull or many bulls, so appropriate output is produced
            System.out.print("1 bull and ");
        } else {
            System.out.print(checker(playerGuess, computerCode)[0] + " bulls and ");
        }
        if (checker(playerGuess, computerCode)[1] == 1) { //Checks to see if we have 1 cow or many cows, so appropriate output is produced
            System.out.println("1 cow");
        } else {
            System.out.println(checker(playerGuess, computerCode)[1] + " cows");
        }
        return playerGuess;
    }

    void computerGuess(String computerGuess, String userCode) {
        //This method provides the output for the computers' guess
        System.out.println("Computer guess: " + computerGuess);
        if (checker(computerGuess, userCode)[0] == 1) { //Checks to see if we have 1 bull or many bulls, so appropriate output is produced
            System.out.print("1 bull and ");
        } else {
            System.out.print(checker(computerGuess, userCode)[0] + " bulls and ");
        }
        if (checker(computerGuess, userCode)[1] == 1) { //Checks to see if we have 1 cow or many cows, so appropriate output is produced
            System.out.println("1 cow");
        } else {
            System.out.println(checker(computerGuess, userCode)[1] + " cows");
        }
    }

    List<String> fileChecker() {
        // This method checks to see if the user wants to extract guesses from a file
        System.out.println("If you would like to use a file for automatic guesses, please type the filename now. Otherwise, press enter to continue");
        String fileName = Keyboard.readInput();
        List<String> guesses = new ArrayList<>();
        if (fileName.length() != 0) {
            guesses = guessReader(fileName); //Calls the guessReader method to obtain the guesses in the file as a list
            while (guesses.size() == 0) {
                //If no guesses are added to the list, due to an exception or because the files contains no guesses, the user is invited to try the filename again
                fileName = Keyboard.readInput();
                if (fileName.length() == 0) {
                    break; //The user is given the opportunity to abandon entering a file name
                }
                guesses = guessReader(fileName);
            }
        }
        return guesses;
    }

    private List<String> guessReader(String filename) {
        // This method reads the guesses from a file, with a specified filename
        // AS STATED, IT IS ASSUMED EACH LINE OF THE FILE CONTAINS VALID INPUT
        List<String> guesses = new ArrayList<>();
        File myFile = new File(filename);
        try (BufferedReader reader = new BufferedReader(new FileReader(myFile))) {

            String line = null;
            while ((line = reader.readLine()) != null) {
                guesses.add(line);
            }
        } catch (IOException e) { //If something is wrong with the file, the user is prompted.
            System.out.println("Invalid File Name. Did you remember to type the directory?");
            System.out.println("Try again, or press enter to continue without a file");
        }
        return guesses; //Returns a List of the guesses in the file
    }

    String guessLooper(List<String> guesses, String playerGuess, String computerCode, int i) {
        //This method is executed in the AI loops to obtain the player's guess in the appropriate way
        if(guesses.size() != 0) {
            try {
                playerGuess = playerGuess(computerCode, guesses.get(i));
            }
            catch (IndexOutOfBoundsException e) { //Allows the player to keep entering guesses should the file run out
                System.out.println("No more guesses in file! You'll need to guess yourself");
                guesses.clear();
                return "NMG";
            }
        }
        else {
            playerGuess = playerGuess(computerCode);
        }
        return playerGuess; //Returns the player's guess to be used in the respective AI
    }

    void saveGame(String userCode, String computerCode, List<String> userGuesses, List<String> computerGuesses, List<int[]> userResults, List<int[]> computerResults, int turns) {
        // This method asks the user if they wish to save the game, and is passed the history of the game.
        boolean save = false;
        System.out.println("Would you like to save your game? (y/n)");
        String input = Keyboard.readInput();
        while(true) {
            // Validates the user input and determines if the game is to be saved
            if (input.toLowerCase().equals("y") || input.toLowerCase().equals("yes")) {
                save = true;
                break;
            }
            else if (input.toLowerCase().equals("n") || input.toLowerCase().equals("no")) {
                System.out.println("Your game will not be saved");
                break;
            }
            else {
                System.out.println("That is not a valid option, please try again");
                input = Keyboard.readInput();
            }
        }
        if (save) {
            //Is executed if the game is to be saved
            System.out.println("Enter a filename:");
            String filename = Keyboard.readInput();
            System.out.println("Your saved game will appear after you exit the game");
            System.out.println();
            File myFile = new File(filename);
            try (BufferedWriter bW = new BufferedWriter(new FileWriter(myFile))) {
                bW.write("Bulls & Cows game result.");
                bW.newLine();
                bW.write("Your code: " + userCode); //Writes the user code
                bW.newLine();
                bW.write("Computer's code: " + computerCode); //Writes the computer code
                bW.newLine();
                bW.write("---");
                bW.newLine();
                for (int i = 0; i < turns; i++) {
                    //Loops through every turn that took place, unless someone won in which case the last turn is excluded
                    bW.write("Turn " + (i+1) + ":");
                    bW.newLine();
                    bW.write(resultWriter(userGuesses.get(i), userResults.get(i)[0], userResults.get(i)[1], "You"));
                    bW.newLine();
                    bW.write(resultWriter(computerGuesses.get(i), computerResults.get(i)[0], computerResults.get(i)[1], "Computer"));
                    bW.newLine();
                    /* The above lines take the information provided from the game history and write it to the file.
                     * They also check for 1 or many bulls/cows so the appropriate syntax is displayed.
                     */
                    bW.write("---");
                    bW.newLine();
                }
                if(userGuesses.size() > computerGuesses.size()) {
                    //Checks if the user has more guesses than the computer, which would be the case if the user won
                    bW.write("Turn " + userGuesses.size() + ":"); //Writes the final turn
                    bW.newLine();
                    bW.write(resultWriter(userGuesses.get(userGuesses.size() - 1), userResults.get(userGuesses.size() - 1)[0], userResults.get(userGuesses.size() - 1)[1], "You"));
                    bW.newLine();
                    /* The above writes the result of the final turn where the user one.
                     * They also check for 1 or many bulls/cows so the appropriate syntax is displayed.
                     * No computer guess was performed so it is not written
                    */
                    bW.write("You win! :)");
                }
                else if (computerResults.get(computerResults.size()-1)[0] == 4) {
                    //Checks if the computer has 4 'bulls' (and has therefore won the game)
                    bW.write("Turn " + userGuesses.size() + ":"); //Writes the final turn
                    bW.newLine();
                    bW.write(resultWriter(userGuesses.get(userGuesses.size() - 1), userResults.get(userGuesses.size() - 1)[0], userResults.get(userGuesses.size() - 1)[1], "You"));
                    bW.newLine();
                    bW.write(resultWriter(computerGuesses.get(computerGuesses.size() - 1), computerResults.get(computerGuesses.size() - 1)[0], computerResults.get(computerGuesses.size() - 1)[1], "Computer"));
                    bW.newLine();
                    /* The above writes the result of the final turn where the computer one.
                     * They also check for 1 or many bulls/cows so the appropriate syntax is displayed.
                     * Both guesses are written
                     */
                    bW.write("Computer wins! :(");
                }
                else {
                    bW.write("Draw!");
                    /*If neither condition is satisfied then no one has won, and the output is handled fully
                    * by the first loop since the number of turns is incremented after a draw
                    * */
                }

            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }

    private String pluralChecker(int value) {
        /* This method checks to see if we need to use the plural for bull(s) and cow(s) and returns the right
        /* string for writing to a file
         */
        if (value == 1) {
            return "";
        }
        else {
            return "s";
        }
    }

    private String resultWriter(String guess, int bullResult, int cowResult, String name) {
        // This method creates the text to be written to file
        return (name +" guessed " + guess + ", scoring " + bullResult
                + " bull" + pluralChecker(bullResult)
                + " and " + cowResult + " cow" + pluralChecker(cowResult));
    }

    int getTurns() {
        //BONUS
        //This method obtains the number of turns the player wishes to make
        int turns = 0;
        while (true) {
            try {
                System.out.println("How many turns would you like before the game ends in a draw?");
                turns = Integer.parseInt(Keyboard.readInput());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input, try again");
            }
        }
        return turns;
    }
}