import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HardAI extends BullCowGameInputOutput {
    //This class inherits all the methods of the BullCowGameInputOutput class
    public void hardAI(){
        // This methods executes the hard version of the game
        System.out.println("Please enter your secret code:");
        String userCode = getUserInput(); //Obtains the users' secret code
        System.out.println("---");
        String computerCode = computerNumber(); //Obtain the computers' secret code
        List<String> possibilities = possibilities(); //Creates a list of every possible code
        List<String> guesses = fileChecker(); //See if the user wants to use guesses from a file
        String computerGuess = possibilities.get((int) (Math.random() * possibilities.size()));
        //The above picks a random value from the list of possibilities for the computer's guess
        List<String> userGuesses = new ArrayList<>(); //For tracking user guess history
        List<String> computerGuesses = new ArrayList<>(); //For tracking computer guess history
        computerGuesses.add(computerGuess); //Adds the initial computer guess to history
        List<int[]> userResults = new ArrayList<>(); //For tracking user result history
        List<int[]> computerResults = new ArrayList<>(); //For tracking computer result history
        int turns = 0; //For tracking the number of turns
        int gameTurns = getTurns(); //For checking how many turns the user wants
        for (int i = 0; i < gameTurns; i++) {
            //Game is running depending on number of turns user wants
            String playerGuess = "";
            playerGuess = guessLooper(guesses, playerGuess, computerCode, i); //Obtains player guess in appropriate way
            if (playerGuess.equals("NMG")) { //If a file is used this will be returned if no more guesses are available
                playerGuess = playerGuess(computerCode); //We ask the player to input their own guess
            }
            userGuesses.add(playerGuess); //The players' guess is added to the history
            userResults.add(checker(playerGuess, computerCode)); //The result of the guess is added to the history
            if (playerGuess.equals(computerCode)) { //If the player has guessed correctly, the game ends
                System.out.println("You win! :)");
                break;
            }
            System.out.println();
            computerGuess(computerGuess, userCode); //The computer guess is checked and the results displayed
            computerResults.add(checker(computerGuess, userCode)); //The result of the computer guess is added to history
            if (computerGuess.equals(userCode)) { //If the computer has guessed correctly the game ends
                System.out.println("Computer wins! :(");
                break;
            }
            possibilities = newPossibilities(possibilities, computerGuess, checker(computerGuess, userCode));
            /* The list of possible codes is updated based on the previous list of possible codes, the guess we made,
             * and the outcome of the guess
             */
            computerGuess = possibilities.get((int) (Math.random()*possibilities.size()));
            //The above selects a new guess from the list of possible codes
            computerGuesses.add(computerGuess); //The new computer guess is added to history
            System.out.println("---");
            if (i == gameTurns-1) { //If the maximum number of turns is reached we have a draw
                System.out.println("Draw!");
                System.out.println();
            }
            turns++; //Increments the turn by 1, but only if no one has won yet
        }
        saveGame(userCode, computerCode, userGuesses, computerGuesses, userResults, computerResults, turns);
        //The above passes the game history to the saveGame method which asks if the user wants it saved, and then does so.
    }

    private List<String> possibilities() {
        /* This methods generates a list of 4 digit numbers, whereby each digit is between 0 and 9, and no number
         * contains the same digit twice
         */
        List<String> possibilities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (i != j) {
                    for (int k = 0; k < 10; k++) {
                        if (k != j && k != i) {
                            for (int l = 0; l < 10; l++) {
                                if (l != k && l != j && l != i) {
                                    possibilities.add(Integer.toString(i) + Integer.toString(j) + Integer.toString(k) + Integer.toString(l));
                                }
                            }
                        }
                    }
                }
            }
        }
        return possibilities;
    }

    private List<String> newPossibilities(List<String> possibilities, String guess, int[] bullCow) {
        //This method creates a new list based on our previous list, guess, and result from the guess
        List<String> toRemove = new ArrayList<>(); //A list of values to be removed is created
        for (String str : possibilities) { //We loop through every value in the list of possibilities
            if (!Arrays.equals(checker(guess,str),bullCow)) {
                /* The above checks our guess against every value in the list of possibilities. If the result of the guess
                 * does not match the result we would expect from that value, then it is added to the list of values to
                 * be removed
                 */
                toRemove.add(str);
            }
        }
        possibilities.removeAll(toRemove);
        // The above removes all of the values we know cannot be the code
        return possibilities;
        // A new list is returned, which narrows down the possible value of the user code
    }
}